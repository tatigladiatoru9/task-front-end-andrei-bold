var messages = [];
function validateForm(){

  let x = new RegExp("^[0-9]*$");
  let y = new String("07");
  let z = new String("+40");
  let v = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");
  let b = new RegExp("[.pdf.PDF]");

const name = document.getElementById("name");
const surname = document.getElementById("surname");
const phoneNumber = document.getElementById("phoneNumber");
const email = document.getElementById("email");
const university = document.getElementById("university");
const studyYear = document.getElementById("studyYear");
const fbProfile = document.getElementById("fbProfile");
const txtArea = document.getElementById("txtArea");
const cv = document.getElementById("cv");
const gdpr = document.getElementById("gdprPolicy");


if (name.value === "" || name.value == null || name.length < 2 || x.test(name.value)) {
  messages.push( "Name is required, must containt at least 2 characters and only letters!");
}

 if (surname.value === "" || surname.value == null || surname.length < 2 || x.test(surname.value) ) {
messages.push( "Surname is required, must containt at least 2 characters and only letters!");
}

 if (phoneNumber.value === "" || phoneNumber.value == null || ((phoneNumber.value.startsWith(y))!=true && (phoneNumber.value.startsWith(z))!=true)) {
messages.push( "Phone number is required and must be 07/+40");
}

if(email.value === "" || email.value == null || !(v.test(email.value))) {
  messages.push("Email adress is required!")
}

if (university.value === "" || university.value == null) {
  messages.push("University is required!")
}

if(studyYear.value === "" || studyYear.value == null || !(x.test(studyYear.value))) {
  messages.push("Study year is required and must contain only numbers!")
}

if(fbProfile.value === "" || fbProfile.value == null) {
  messages.push("Facebook profile is required!")
}

if(txtArea.value === "" || txtArea.value == null) {
  messages.push("Text area is required!")
}

if(b.test(cv.value)!=true) {
  messages.push("Resume must be uploaded!")
}

if(gdpr.checked!=true) {
  messages.push("You must agree with the GDPR Policy!")
}

return messages.length == 0;
}




 document
  .getElementById("submitBtn")
  .addEventListener("click", function() {
    if (validateForm() == false)
    {
      messages.forEach ( item =>{
        toastr.error(item);
      });
      
    }
    else {
      toastr.success("Congratz! One step closer to becoming CroCoder!:D")
    }
   
  }); 
